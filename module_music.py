from header_music import *
####################################################################################################################
#  Each track record contains the following fields:
#  1) Track id: used for referencing tracks.
#  2) Track file: filename of the track
#  3) Track flags. See header_music.py for a list of available flags
#  4) Continue Track flags: Shows in which situations or cultures the track can continue playing. See header_music.py for a list of available flags
####################################################################################################################

# WARNING: You MUST add mtf_module_track flag to the flags of the tracks located under module directory

tracks = [
##  ("losing_battle", "alosingbattle.mp3", sit_calm|sit_action),
##  ("reluctant_hero", "reluctanthero.mp3", sit_action),
##  ("the_great_hall", "thegreathall.mp3", sit_calm),
##  ("requiem", "requiem.mp3", sit_calm),
##  ("silent_intruder", "silentintruder.mp3", sit_calm|sit_action),
##  ("the_pilgrimage", "thepilgrimage.mp3", sit_calm),
##  ("ambushed", "ambushed.mp3", sit_action),
##  ("triumph", "triumph.mp3", sit_action),

##  ("losing_battle", "alosingbattle.mp3", mtf_sit_map_travel|mtf_sit_attack),
##  ("reluctant_hero", "reluctanthero.mp3", mtf_sit_attack),
##  ("the_great_hall", "thegreathall.mp3", mtf_sit_map_travel),
##  ("requiem", "requiem.mp3", mtf_sit_map_travel),
##  ("silent_intruder", "silentintruder.mp3", mtf_sit_map_travel|mtf_sit_attack),
##  ("the_pilgrimage", "thepilgrimage.mp3", mtf_sit_map_travel),
##  ("ambushed", "ambushed.mp3", mtf_sit_attack),
##  ("triumph", "triumph.mp3", mtf_sit_attack),
  ("bogus", "cant_find_this.ogg", 0, 0),
  ("mount_and_blade_title_screen", "wiedzmin_soundtrack_1.mp3", mtf_sit_main_title|mtf_start_immediately, 0),

  ("ambushed_by_neutral", "wiedzmin_soundtrack_8.mp3", mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight),
  ("ambushed_by_khergit", "wiedzmin_soundtrack_8.mp3", mtf_culture_3|mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_culture_all),
  ("ambushed_by_nord",    "wiedzmin_soundtrack_8.mp3", mtf_culture_4|mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_culture_all),
  ("ambushed_by_rhodok",  "wiedzmin_soundtrack_8.mp3", mtf_culture_5|mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_culture_all),
  ("ambushed_by_swadian", "wiedzmin_soundtrack_8.mp3", mtf_culture_1|mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_culture_all),
  ("ambushed_by_vaegir",  "wiedzmin_soundtrack_8.mp3", mtf_culture_2|mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_culture_all),
  ("ambushed_by_sarranid", "wiedzmin_soundtrack_8.mp3", mtf_culture_6|mtf_sit_ambushed|mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_culture_all),
  
  ("arena_1", "wiedzmin_soundtrack_1.mp3", mtf_sit_arena, 0),
#  ("arena_2", "arena_2.ogg", mtf_looping|mtf_sit_arena, 0),
  ("armorer", "wiedzmin_soundtrack_25.mp3", mtf_sit_travel, 0),
  ("bandit_fight", "wiedzmin_soundtrack_10.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),

  ("calm_night_1", "wiedzmin_soundtrack_21.mp3", mtf_sit_night, mtf_sit_town|mtf_sit_tavern|mtf_sit_travel),
  ("captured", "wiedzmin_soundtrack_22.mp3", mtf_persist_until_finished, 0),
  ("defeated_by_neutral", "wiedzmin_soundtrack_22.mp3",mtf_persist_until_finished|mtf_sit_killed, 0),
  ("defeated_by_neutral_2", "wiedzmin_soundtrack_22.mp3", mtf_persist_until_finished|mtf_sit_killed, 0),
  ("defeated_by_neutral_3", "wiedzmin_soundtrack_22.mp3", mtf_persist_until_finished|mtf_sit_killed, 0),

  ("empty_village", "wiedzmin_soundtrack_2.mp3", mtf_persist_until_finished, 0),
  ("encounter_hostile_nords", "wiedzmin_soundtrack_16.mp3", mtf_persist_until_finished|mtf_sit_encounter_hostile, 0),
  ("escape", "wiedzmin_soundtrack_16.mp3", mtf_persist_until_finished, 0),

  ("fight_1", "wiedzmin_soundtrack_6.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),
  ("fight_2", "wiedzmin_soundtrack_10.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),
  ("fight_3", "wiedzmin_soundtrack_14.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),
  ("ambient_forest", "amb_forest.ogg", mtf_sit_fight, 0),
  ("ambient_swamp", "amb_swamp.ogg", mtf_sit_fight, 0),
  ("rain_thunder_loop", "rain_thunder_loop.wav", 0, 0),
  ("fight_as_khergit", "wiedzmin_soundtrack_10.mp3", mtf_culture_3|mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, mtf_culture_all),
  ("fight_4", "wiedzmin_soundtrack_15.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),  
  ("fight_as_nord", "wiedzmin_soundtrack_6.mp3", mtf_culture_4|mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, mtf_culture_all),
  ("fight_as_rhodok", "wiedzmin_soundtrack_10.mp3", mtf_culture_5|mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, mtf_culture_all),
#  ("fight_as_swadian", "fight_as_swadian.ogg", mtf_culture_1|mtf_sit_fight|mtf_sit_multiplayer_fight, mtf_culture_all),
  ("fight_as_vaegir", "wiedzmin_soundtrack_14.mp3", mtf_culture_2|mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, mtf_culture_all),
  ("fight_while_mounted_1", "wiedzmin_soundtrack_15.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),
  ("fight_while_mounted_2", "wiedzmin_soundtrack_14.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),
  ("fight_while_mounted_3", "wiedzmin_soundtrack_6.mp3", mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed, 0),
  
  ("infiltration_khergit", "wiedzmin_soundtrack_26.mp3", mtf_culture_3|mtf_sit_town_infiltrate, mtf_culture_all),

  ("killed_by_khergit", "wiedzmin_soundtrack_16.mp3", mtf_persist_until_finished|mtf_culture_3|mtf_sit_killed, 0),
#  ("killed_by_neutral", "killed_by_neutral.ogg", mtf_persist_until_finished|mtf_culture_6|mtf_sit_killed, 0),
#  ("killed_by_nord", "killed_by_nord.ogg", mtf_persist_until_finished|mtf_culture_4|mtf_sit_killed, 0),
#  ("killed_by_rhodok", "killed_by_rhodok.ogg", mtf_persist_until_finished|mtf_culture_5|mtf_sit_killed, 0),
  ("killed_by_swadian", "wiedzmin_soundtrack_16.mp3", mtf_persist_until_finished|mtf_culture_1|mtf_sit_killed, 0),
#  ("killed_by_vaegir", "killed_by_vaegir.ogg", mtf_persist_until_finished|mtf_culture_2|mtf_sit_killed, 0),
  
  ("lords_hall_khergit", "wiedzmin_soundtrack_13.mp3", mtf_culture_3|mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern|mtf_culture_all),
  ("lords_hall_nord", "wiedzmin_soundtrack_13.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),
  ("lords_hall_swadian", "wiedzmin_soundtrack_13.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),
  ("lords_hall_rhodok", "wiedzmin_soundtrack_13.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),
  ("lords_hall_vaegir", "wiedzmin_soundtrack_13.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),

  ("mounted_snow_terrain_calm", "wiedzmin_soundtrack_25.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_night|mtf_sit_tavern),
  ("neutral_infiltration", "wiedzmin_soundtrack_26.mp3", mtf_sit_town_infiltrate, 0),
  ("outdoor_beautiful_land", "wiedzmin_soundtrack_25.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_night|mtf_sit_tavern),
  ("retreat", "wiedzmin_soundtrack_16.mp3", mtf_persist_until_finished|mtf_sit_killed, 0),

  ("seige_neutral", "wiedzmin_soundtrack_24.mp3", mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed),
  ("enter_the_juggernaut", "wiedzmin_soundtrack_24.mp3", mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed),  
  ("siege_attempt", "wiedzmin_soundtrack_24.mp3", mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed),  
  ("crazy_battle_music", "wiedzmin_soundtrack_5.mp3", mtf_sit_siege, mtf_sit_fight|mtf_sit_multiplayer_fight|mtf_sit_ambushed),
  
  ("tavern_1", "wiedzmin_soundtrack_7.mp3", mtf_sit_tavern|mtf_sit_feast, 0),
  ("tavern_2", "wiedzmin_soundtrack_4.mp3", mtf_sit_tavern|mtf_sit_feast, 0),

  ("town_neutral", "wiedzmin_soundtrack_18.mp3", mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night),
  ("town_neutral_2", "wiedzmin_soundtrack_19.mp3", mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night),
  ("town_khergit", "wiedzmin_soundtrack_18.mp3", mtf_culture_3|mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("town_nord", "wiedzmin_soundtrack_18.mp3", mtf_culture_4|mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("town_rhodok", "wiedzmin_soundtrack_18.mp3", mtf_culture_5|mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("town_swadian", "wiedzmin_soundtrack_18.mp3", mtf_culture_1|mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("town_vaegir", "wiedzmin_soundtrack_18.mp3", mtf_culture_2|mtf_sit_town|mtf_sit_travel, mtf_sit_tavern|mtf_sit_night|mtf_culture_all),

  ("travel_khergit", "wiedzmin_soundtrack_3.mp3", mtf_culture_3|mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("travel_neutral", "wiedzmin_soundtrack_9.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night),
  ("travel_nord",    "wiedzmin_soundtrack_3.mp3",    mtf_culture_4|mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("travel_rhodok",  "wiedzmin_soundtrack_3.mp3",  mtf_culture_5|mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("travel_swadian", "wiedzmin_soundtrack_9.mp3", mtf_culture_1|mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("travel_vaegir",  "wiedzmin_soundtrack_3.mp3",  mtf_culture_2|mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night|mtf_culture_all),
  ("travel_sarranid",  "wiedzmin_soundtrack_3.mp3",  mtf_culture_6|mtf_sit_travel, mtf_sit_town|mtf_sit_tavern|mtf_sit_night|mtf_culture_all),

  ("uncertain_homestead", "wiedzmin_soundtrack_23.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),
  ("hearth_and_brotherhood", "wiedzmin_soundtrack_23.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),
  ("tragic_village", "wiedzmin_soundtrack_20.mp3", mtf_sit_travel, mtf_sit_town|mtf_sit_night|mtf_sit_tavern),

  ("victorious_evil", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished, 0),
  ("victorious_neutral_1", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished|mtf_sit_victorious, 0),
  ("victorious_neutral_2", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished|mtf_sit_victorious, 0),
  ("victorious_neutral_3", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished|mtf_sit_victorious, 0),

  ("victorious_swadian", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished|mtf_culture_2|mtf_sit_victorious, 0),
  ("victorious_vaegir", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished|mtf_culture_2|mtf_sit_victorious, 0),
  ("victorious_vaegir_2", "wiedzmin_soundtrack_17.mp3", mtf_persist_until_finished|mtf_culture_2|mtf_sit_victorious, 0),
  ("wedding", "wedding.ogg", mtf_persist_until_finished, 0),

  ("coronation", "coronation.ogg", mtf_persist_until_finished, 0),
  ("crystalmoors_mons_vindivs", "crystalmoors_mons_vindivs.wav", mtf_module_track, 0),




  
]
