# -*- coding: UTF-8 -*-
from header_common import *
from header_parties import *
from ID_troops import *
from ID_factions import *
from ID_party_templates import *
from ID_map_icons import *

####################################################################################################################
#  Each party record contains the following fields:
#  1) Party id: used for referencing parties in other files.
#     The prefix p_ is automatically added before each party id.
#  2) Party name.
#  3) Party flags. See header_parties.py for a list of available flags
#  4) Menu. ID of the menu to use when this party is met. The value 0 uses the default party encounter system.
#  5) Party-template. ID of the party template this party belongs to. Use pt_none as the default value.
#  6) Faction.
#  7) Personality. See header_parties.py for an explanation of personality flags.
#  8) Ai-behavior
#  9) Ai-target party
# 10) Initial coordinates.
# 11) List of stacks. Each stack record is a triple that contains the following fields:
#   11.1) Troop-id.
#   11.2) Number of troops in this stack.
#   11.3) Member flags. Use pmf_is_prisoner to note that this member is a prisoner.
# 12) Party direction in degrees [optional]
####################################################################################################################

no_menu = 0
#pf_town = pf_is_static|pf_always_visible|pf_hide_defenders|pf_show_faction
pf_town = pf_is_static|pf_show_faction|pf_label_large
pf_town_small = pf_is_static|pf_show_faction|pf_label_medium
pf_castle = pf_is_static|pf_show_faction|pf_label_medium
pf_village = pf_is_static|pf_hide_defenders|pf_label_small

#sample_party = [(trp_swadian_knight,1,0), (trp_swadian_peasant,10,0), (trp_swadian_crossbowman,1,0), (trp_swadian_man_at_arms, 1, 0), (trp_swadian_footman, 1, 0), (trp_swadian_militia,1,0)]

# NEW TOWNS:
# NORMANDY: Rouen, Caen, Bayeux, Coutances, Evreux, Avranches
# Brittany: Rennes, Nantes,
# Maine: Le Mans
# Anjou: Angers


parties = [
  ("main_party","Main Party",icon_player|pf_limit_members, no_menu, pt_none,fac_player_faction,0,ai_bhvr_hold,0,(17, 52.5),[(trp_player,1,0)]),
  ("temp_party","{!}temp_party",pf_disabled, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0,0),[]),
  ("camp_bandits","{!}camp_bandits",pf_disabled, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(1,1),[(trp_temp_troop,3,0)]),
#parties before this point are hardwired. Their order should not be changed.

  ("temp_party_2","{!}temp_party_2",pf_disabled, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0,0),[]),
#Used for calculating casulties.
  ("temp_casualties","{!}casualties",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("temp_casualties_2","{!}casualties",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("temp_casualties_3","{!}casualties",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("temp_wounded","{!}enemies_wounded",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("temp_killed", "{!}enemies_killed", pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("main_party_backup","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("encountered_party_backup","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
#  ("ally_party_backup","_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("collective_friends_backup","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("player_casualties","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("enemy_casualties","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("ally_casualties","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),

  ("collective_enemy","{!}collective_enemy",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  #TODO: remove this and move all to collective ally
  ("collective_ally","{!}collective_ally",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),
  ("collective_friends","{!}collective_ally",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),

  ("total_enemy_casualties","{!}_",  pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]), #ganimet hesaplari icin #new:
  ("routed_enemies","{!}routed_enemies",pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]), #new:

#  ("village_reinforcements","village_reinforcements",pf_is_static|pf_disabled, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1,1),[]),

###############################################################
  ("karczma","Karczma Jelen",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(18,60),[]),
  ("karczma_2","Karczma Zbik",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(20,60),[]),
  ("karczma_3","Karczma pod Dzikiem",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(22,63),[]),
  ("karczma_4","Ognisko Beznadziei",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(20,63),[]),
  ("bagna","Bagna",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(25,63),[]),
  ("plaza","Plaza",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(28,63),[]),
  ("zendar","Zendar",pf_disabled|icon_town|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(18,60),[]),
  ("oboz_mysliwych","Oboz Mysliwych",icon_camp|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-46.8,67.8),[]),

  ("town_1","Cedynia",  icon_town_big|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-113.76,46.36),[], 170),
  ("town_2","Wolin",     icon_wolin|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-109.83,73.47),[], 165),
  ("town_3","Plock",   icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(44.5, 23),[], 80),
  ("town_4","Gniezno",     icon_gniezno|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-39.66,16.12),[], 110),
  ("town_5","Grzebsk",  icon_town|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(62,38),[], 90),
  ("town_6","Grzybowo",   icon_town_big|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-48, 1.14),[], 155),
  ("town_7","Poznan",   icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-62.24,7),[], 140),

  ("town_8","Opole", icon_town_big|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-52.11,-62.12),[], 175),
  ("town_9","Wroclaw",   icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-78.62,-31.95),[], 270),
  ("town_10","Wislica",   icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(33.65,-72.14),[], 310),
  ("town_11","Legnica",   icon_town|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-110,-29.5),[], 150),
  ("town_12","Gdansk", icon_gdansk|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(8.03,88.66),[], 0),
  ("town_13","Glogow",icon_town|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-107.4,-8),[], 240),
  ("town_14","Krakow",  icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1.4,-97.9),[], 135),

  ("town_15","Czersk",  icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(81.93, -20.16),[], 45),
  ("town_16","Santok",  icon_town|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-99.96,35.93),[], 90),
  ("town_17","Trzczinica",  icon_town|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(50,-106),[], 90),
  ("town_18","Naszacowice",  icon_town_big|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(24.25,-120.74),[], 135),

  ("town_19","Sieradz", icon_town_big|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-1.1,-22.07),[], 45),
  ("town_20","Kolobrzeg", icon_town|pf_town_small, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-71.5,87),[], 270),
  ("town_21","Swieck", icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(120,23),[], 330),
  ("town_22","Lubomia", icon_town|pf_town, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-61,-105),[], 225),

# Polanie
  ("castle_1","Ujscie",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-57.75, 43.47),[],50),
  ("castle_2","Wiecbork",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-37,60),[],75),
  ("castle_3","Lad",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-16.25, -14.34),[],100),

# Pomorzanie
  ("castle_4","Stargard",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-97.10, 52.33),[],180),
  ("castle_5","Slupsk",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-4.8, 63.7),[],90),
  ("castle_6","Bialogard",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-68.36, 75.90),[],55),

# Mazowszanie
  ("castle_7","Leczyca",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(32, -6.5),[],45),
  ("castle_8","Pultusk",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(79, 24),[],30),
  ("castle_9","Wizna",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(115.2, 38.3),[],100),

# Slezanie
  ("castle_10","Zary",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-130.23, -12.50),[],110),
  ("castle_11","Niemcza",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-86.26, -50.43),[],75),
  ("castle_12","Ryczyn mod",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-65.5, -45.5),[],95),

# Wislanie
  ("castle_13","Biecz",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(102.10,-112.2),[],115),
  ("castle_14","Tyniec",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-16.09,-101.7),[],90),
  ("castle_15","Sacz",icon_grod_mniejszy|pf_castle, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(13.32,-115.43),[],235),




#     Rinimad
#              Rietal Derchios Gerdus
# Tuavus   Pamir   vezona

  ("village_1", "Miodniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-19.4,-101.5),[], 100),# Wislanie
  ("village_2", "Drewniary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-4.42,-94.98),[], 110),
  ("village_3", "Bartodzieje",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(9,-101.7),[], 120),
  ("village_4", "Bobrowniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(15.5,-90),[], 130),
  ("village_5", "Cienietniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(28,-123),[], 170),
  ("village_6", "Kolodzieje",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(1.56,-134.72),[], 100),#gorska
  ("village_7", "Ciesle",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(13.14,-119.5),[], 110),
  ("village_8", "Grotniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(36.54,-111.11),[], 120),
  ("village_9", "Jadowniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(18.22,-98.4),[], 130),
  ("village_10","Chmielary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(59,-108),[], 170),

  ("village_11","Psary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(97.5,-114.6),[], 100),
  ("village_12","Ruydniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(87.2,-95),[], 110),
  ("village_13","Sokolniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(14.64,-92.2),[], 120),
  ("village_14","Swiniary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(31.62,-78.6),[], 130),
  ("village_15","Klobuck",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(41,-73.24),[], 170),
  ("village_16","Garnckarsko",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(24.9,-111.11),[], 170),
  ("village_17","Kowale",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(17.12,-80),[], 35),
  ("village_18","Miodary",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(0,-108),[], 170),
  ("village_19","Kuchary",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(8,-82.8),[], 170),
  ("village_20","Zduny",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(68.8,-82),[], 170),

  ("village_21","Szewce",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(72.6,-103.2),[], 100),
  ("village_22","Solacz",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(39,-121),[], 110),
  ("village_23","Kuchary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-112, 48.36),[], 120),# Pomorzanie
  ("village_24","Mydlniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(57.6, -73),[], 130),
  ("village_25","Piekary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-109.6,43.5),[], 170),
  ("village_26","Pracze",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-106, 59),[], 170),
  ("village_27","Glunmar",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-91,50),[], 170),
  ("village_28","Cienietniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-107,69),[], 170),
  ("village_29","Zduny",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-101.54,73.9),[], 170),

  ("village_30","Rybacze",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-98, 78.55),[], 100),
  ("village_31","Budy",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-83.92,83.56),[], 100),
  ("village_32","Kobylany",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-74.10,82.53),[], 110),
  ("village_33","Kowary",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-65.5,87),[], 120),
  ("village_34","Srebrniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-94.6, 61),[], 130),
  ("village_35","Szczytniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(0,60),[], 170),
  ("village_36","Szewce",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(5.8,81),[], 170),
  ("village_37","Kobylniki Small",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-1.37,87.5),[], 170),
  ("village_38","Kawle",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-7.71,106.13),[], 170),
  ("village_39","Rybitwy",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-21.53, 106),[], 110),
  ("village_40","Szczytniki Small",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-23.2,95.4),[], 170),

  ("village_41","Szczytniki Big",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-88.5,69),[], 100),
  ("village_42","Zerniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-6.3, 98.4),[], 110),
  ("village_43","Zerniki Small",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-77.4,76),[], 120),
  ("village_44","Old Wolka",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-55,91.8),[], 130),
  ("village_45","Lisie Doły",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-98.5, 38),[], 170),# Polanie
  ("village_46","Wrzosowa",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-100.8, 31),[], 170),
  ("village_47","Lutowice",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-83.4, 29.75),[], 170),
  ("village_48","Polanka mod",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-68, 3),[], 170),
  ("village_49","Tismirr",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(60.3, 14.4),[], 10),
  ("village_50","Chorowice",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-73.7, 3),[], 170),

  ("village_51","Łapczyce",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-59,38.6),[], 100),
  ("village_52","Kłomnica",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-57.5, 50),[], 110),
  ("village_53","Biały Sad",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-31.7,60),[], 120),
  ("village_54","Odmęty",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-40, 25),[], 130),
  ("village_55","Zębocin",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-32, 7.6),[], 170),
  ("village_56","Zdrody mod",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-21.5, 9.4),[], 170),
  ("village_57","Drogwino",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-22.2, -20),[], 170),
  ("village_58","Żędziany",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-24.26, -8.4),[], 170),
  ("village_59","Prądnik",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-1.7, -11.11),[], 170),
  ("village_60","Zagorzany",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(6.5, -30),[], 170),

  ("village_61","Dolany mod",  icon_village_c|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-9, -33.5),[], 100),
  ("village_62","Brzeżany",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-2, 7),[], 100),
  ("village_63","Jezierzany",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-41.5, -15.3),[], 100),
  ("village_64","Szkalbmierz",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-27, 3),[], 100),
  ("village_65","Bogusze",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-13, 24),[], 100),
  ("village_66","Cibory",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-37, 41),[], 100),# Mazowszanie
  ("village_67","Tybory",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(57, 41),[], 100),
  ("village_68","Ślepowrony",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(70, 39),[], 100),
  ("village_69","Konopki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(59, 32),[], 100),
  ("village_70","Zatory",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(32,20),[], 100),

  ("village_71","Kruki mod",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(46, 9.6),[], 20),
  ("village_72","Tykocin",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(62, 0.5),[], 60),
  ("village_73","Sikory",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(23.66, -4.65),[], 55),
  ("village_74","Lipniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(35, 17),[], 15),
  ("village_75","Dąb",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(40.5, 0.7),[], 10),
  ("village_76","Żarnowiec",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(74, -36),[], 35),
  ("village_77","Gawarzec",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(90, -20),[], 160),
  ("village_78","Nasidlsk",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(85, 20),[], 180),
  ("village_79","Chrzanów",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(80, 31.5),[], 0),
  ("village_80","Kamień",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(91, 25),[], 40),

  ("village_81","Śleź mod",  icon_village_c|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(105.4, 32),[], 110),
  ("village_82","Sierpc",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(120, 35.4),[], 60),
  ("village_83","Przypust mod",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(126.5, 19.3),[], 55),
  ("village_84","Zakroczym",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(117, 11.5),[], 15),
  ("village_85","Bystrzyca",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(69, 21),[], 10),
  ("village_86","Wojcin",  icon_village_snow_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(34.2, 36),[], 35),
  ("village_87","Czastary mod",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(38, -17.6),[], 160),
  ("village_88","Kosowo",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-105, 0),[], 180),# Slezanie
  ("village_89","Lubieszewo",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-95.7, -5),[], 0),
  ("village_90","Łysiec",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-108, -15.3),[], 40),

  ("village_91","Czerwińsk",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-121, -9.6),[], 20),
  ("village_92","Wojcice",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-134, -14),[], 60),
  ("village_93","Domanowo",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-126, -17.6),[], 55),
  ("village_94","Gawarce",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-115, -25),[], 15),
  ("village_95","Bystrzyca",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-116.86, -33.8),[], 10),
  ("village_96","Bogdanowo",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-102.72, -30.4),[], 35),
  ("village_97","Świdniki",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-104.4, -46.25),[], 160),
  ("village_98","Sokołowo",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-84, -42.95),[], 180),
  ("village_99","Syrock",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-86, -32),[], 0),
  ("village_100","Włodzisław",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-70, -32.6),[], 40),

  ("village_101","Wojbor",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-66.8, -41.7),[], 20),
  ("village_102","Maków",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-61.6, -47),[], 60),
  ("village_103","Rozprza",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-55, -54),[], 55),
  ("village_104","Rypin",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-61.5, -69),[], 15),
  ("village_105","Sławienin",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-81, -69),[], 10),
  ("village_106","Żabno",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-49.7, -102.5),[], 35),
  ("village_107","Wieczanowo",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-75, -107.6),[], 160),
  ("village_108","Wolborz",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-61.7, -117.39),[], 180),
  ("village_109","Kurzawa",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-68.4, -84.6),[], 0),
  ("village_110","Mały Sad",  icon_village_a|pf_village, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-86, -14.4),[], 40),

  ("salt_mine","Salt_Mine",icon_village_a|pf_disabled|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(14.2, -31),[]),
  ("four_ways_inn","Four_Ways_Inn",icon_village_a|pf_disabled|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(4.8, -39.6),[]),
  ("test_scene","test_scene",icon_village_a|pf_disabled|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(10.8, -19.6),[]),
  #("test_scene","test_scene",icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(10.8, -19.6),[]),
  ("battlefields","battlefields",pf_disabled|icon_village_a|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(10.8, -16.6),[]),
  ("dhorak_keep","Dhorak_Keep",icon_town|pf_disabled|pf_is_static|pf_always_visible|pf_no_label|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-50,-58),[]),

  ("training_ground","Training Ground",  pf_disabled|icon_training_ground|pf_hide_defenders|pf_is_static|pf_always_visible, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(3, -7),[]),

  ("training_ground_1", "Training Field",  icon_training_ground|pf_hide_defenders|pf_is_static|pf_always_visible|pf_label_medium, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-108.75,66),[], 100),
  ("training_ground_2", "Training Field",  icon_training_ground|pf_hide_defenders|pf_is_static|pf_always_visible|pf_label_medium, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(15.15,-82),[], 100),
  ("training_ground_3", "Training Field",  icon_training_ground|pf_hide_defenders|pf_is_static|pf_always_visible|pf_label_medium, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(82.29,16),[], 100),
  ("training_ground_4", "Training Field",  icon_training_ground|pf_hide_defenders|pf_is_static|pf_always_visible|pf_label_medium, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-48.1,11),[], 100),
  ("training_ground_5", "Training Field",  icon_training_ground|pf_hide_defenders|pf_is_static|pf_always_visible|pf_label_medium, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-69.15,-38.92),[], 100),


#  bridge_a
  ("Bridge_1","{!}1",pf_disabled|icon_bridge_snow_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(39.37, 65.10),[], -44.8),
  ("Bridge_2","{!}2",pf_disabled|icon_bridge_snow_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(56.44, 77.88),[], 4.28),
  ("Bridge_3","{!}3",pf_disabled|icon_bridge_snow_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(70.87, 87.95),[], 64.5),
  ("Bridge_4","{!}4",pf_disabled|icon_bridge_snow_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(93.71, 62.13),[], -2.13),
  ("Bridge_5","{!}5",pf_disabled|icon_bridge_snow_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(11.02, 72.61),[], 21.5),
  ("Bridge_6","{!}6",pf_disabled|icon_bridge_b|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-8.83, 52.24),[], -73.5),
  ("Bridge_7","{!}7",pf_disabled|icon_bridge_b|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-29.79, 76.84),[], -64),
  ("Bridge_8","{!}8",pf_disabled|icon_bridge_b|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-64.05, -6),[], 1.72),
  ("Bridge_9","{!}9",pf_disabled|icon_bridge_b|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-64.95, -9.60),[], -33.76),
  ("Bridge_10","{!}10",pf_disabled|icon_bridge_b|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-75.32, -75.27),[], -44.07),
  ("Bridge_11","{!}11",pf_disabled|icon_bridge_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-24.39, 67.82),[], 81.3),
  ("Bridge_12","{!}12",pf_disabled|icon_bridge_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-114.33, -1.94),[], -35.5),
  ("Bridge_13","{!}13",pf_disabled|icon_bridge_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-84.02, -7),[], -17.7),
  ("Bridge_14","{!}14",pf_disabled|icon_bridge_a|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-23.36, 75.8),[], 66.6),
  
  
  ("region_1_1","Pomorze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-103.91,67),[], 66.6),# Pomorze
  ("region_1_2","Pomorze 2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-73, 80),[], 66.6),
  ("region_1_3","Pomorze 3",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-29, 89),[], 66.6),
  ("region_1_4","Pomorze 4",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-3,86),[], 66.6),  
  ("region_2_1","Wielkopolska",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-43,52),[], 66.6),# Wielkopolska
  ("region_2_2","Wielkopolska 2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-91, 32),[], 66.6),
  ("region_2_3","Wielkopolska 3",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-49, 13.6),[], 66.6),
  ("region_2_4","Wielkopolska 4",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-14.94,-15),[], 66.6),
  ("region_3_1","Mazowsze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(37.33,26),[], 66.6),# Mazowsze
  ("region_3_2","Mazowsze 2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(63,-9),[], 66.6),
  ("region_3_3","Mazowsze 3",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(89, -4),[], 66.6),
  ("region_3_4","Mazowsze 4",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(89, 29),[], 66.6),
  ("region_4_1","Ślask",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-112,-12),[], 66.6),# Slask
  ("region_4_2","Slask 2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-89, -36),[], 66.6),
  ("region_4_3","Slask 3",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-73, -45),[], 66.6),
  ("region_4_4","Slask 4",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-85, -93),[], 66.6),
  ("region_5_1","Chrobacja",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-14.6, -102),[], 66.6),# Chrobacja
  ("region_5_2","Chrobacja 2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(14,-89),[], 66.6),
  ("region_5_3","Chrobacja 3",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(51, -108),[], 66.6),
  ("region_5_4","Chrobacja 4",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(54,-76),[], 66.6),
  ("region_6_1","Prusy",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(28,54),[], 66.6),# Slask
  ("region_6_2","Prusy 2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(43, 65),[], 66.6),
  ("region_6_3","Prusy 3",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(39, 80),[], 66.6),
  ("region_6_4","Prusy 4",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(71, 69),[], 66.6),
  ("region_end","{!}2",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-23.36, 75.8),[], 66.6),


  ("sea_1","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-98, 85),[], 66.6),
  ("sea_2","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-84,89),[], 66.6),
  ("sea_3","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-74, 92),[], 66.6),
  ("sea_4","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-64, 94),[], 66.6),
  ("sea_5","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-55,96),[], 66.6),
  ("sea_6","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-42, 100),[], 66.6),
  ("sea_7","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-32, 105),[], 66.6),
  ("sea_8","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-22, 110),[], 66.6),
  ("sea_9","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-8,110),[], 66.6),
  ("sea_end","Morze",pf_disabled|pf_is_static, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-8,110),[], 66.6),

  ("zboze_1","{!}1",icon_ikonka_zboze|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-61, -9.5),[], -44.8),
  ("maczuga_herkulesa","{!}1",icon_ikona_maczuga_herkulesa|pf_is_static|pf_always_visible|pf_no_label, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-18.48, -78.72),[], -44.8),
  ("oboz_bandytow","Obóz Bandytów",icon_bandit_lair|pf_is_static|pf_always_visible|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(18,60),[]),
  ("las_quest","Zachodni Las",pf_is_static|pf_hide_defenders, no_menu, pt_none, fac_neutral,0,ai_bhvr_hold,0,(-52.92,64),[]),

  ("zwierzeta_spawn_point_1", "{!}zwierzeta_sp",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-46.30,72.28),[(trp_looter,15,0)]),
  ("zwierzeta_spawn_point_2", "{!}zwierzeta_sp2",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-42.63,46.74),[(trp_looter,15,0)]),
  ("zwierzeta_spawn_point_3", "{!}zwierzeta_sp3",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(9.82,11.32),[(trp_looter,15,0)]),
  ("zwierzeta_spawn_point_4", "{!}zwierzeta_sp4",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-32.36,-25.43),[(trp_looter,15,0)]),
  ("looter_spawn_point"   ,"{!}looter_sp",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-6,8),[(trp_looter,15,0)]),
  ("steppe_bandit_spawn_point"  ,"the steppes",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(114, -90),[(trp_looter,15,0)]),
  ("taiga_bandit_spawn_point"   ,"the tundra",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-125,-11),[(trp_looter,15,0)]),
##  ("black_khergit_spawn_point"  ,"black_khergit_sp",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(47.1, -73.3),[(trp_looter,15,0)]),
  ("forest_bandit_spawn_point"  ,"the forests",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(40, -34),[(trp_looter,15,0)]),
  ("mountain_bandit_spawn_point","the highlands",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-22, -15.8),[(trp_looter,15,0)]),
  ("sea_raider_spawn_point_1"   ,"the coast",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-94, 74),[(trp_looter,15,0)]),
  ("sea_raider_spawn_point_2"   ,"the coast",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-20, 86.7),[(trp_looter,15,0)]),
  ("desert_bandit_spawn_point"  ,"the deserts",pf_disabled|pf_is_static, no_menu, pt_none, fac_outlaws,0,ai_bhvr_hold,0,(-58, -133),[(trp_looter,15,0)]),
 # add extra towns before this point
  ("spawn_points_end"                  ,"{!}last_spawn_point",    pf_disabled|pf_is_static, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0., 0),[(trp_looter,15,0)]),
  ("reserved_1"                  ,"{!}last_spawn_point",    pf_disabled|pf_is_static, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0., 0),[(trp_looter,15,0)]),
  ("reserved_2"                  ,"{!}last_spawn_point",    pf_disabled|pf_is_static, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0., 0),[(trp_looter,15,0)]),
  ("reserved_3"                  ,"{!}last_spawn_point",    pf_disabled|pf_is_static, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0., 0),[(trp_looter,15,0)]),
  ("reserved_4"                  ,"{!}last_spawn_point",    pf_disabled|pf_is_static, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0., 0),[(trp_looter,15,0)]),
  ("reserved_5"                  ,"{!}last_spawn_point",    pf_disabled|pf_is_static, no_menu, pt_none, fac_commoners,0,ai_bhvr_hold,0,(0., 0),[(trp_looter,15,0)]),
 ]
